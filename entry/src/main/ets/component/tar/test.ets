/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fs from '@ohos.file.fs';
import {
  File,
  OutputStream,
  InputStream,
  IOUtils,
  ArchiveOutputStream,
  TarArchiveInputStream,
  TarConstants,
  ArchiveStreamFactory,
  TarArchiveEntry,
  Long,
  TarArchiveOutputStream
} from '@ohos/commons-compress';
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
struct TarTest {
  @State isCompressTarFileShow: boolean = false;
  @State isDeCompressTarShow: boolean = false;
  @State isPosixDeCompressTarShow: boolean = false;
  @State isGnuDeCompressTarShow: boolean = false;
  @State isLongFileNameCompressTarShow: boolean = false;
  @State newFolder: string = 'newFolderCode';
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text($r('app.string.Tar_functions'))
        .fontSize(20)
        .margin({ top: 16 })

      Text($r('app.string.generate_xml'))
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressTarFileShow) {
        Text($r('app.string.compress_xml_as_tar'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsTarTest()
            }
          })
      }

      if (this.isDeCompressTarShow) {
        Text($r('app.string.decompress_tar'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsUnTarTest()
            }
          })
      }

      Text($r('app.string.Click_on_the_longfilename_file'))
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateLongFileNameTextFile()
          }
        })

      if (this.isLongFileNameCompressTarShow) {
        Text($r('app.string.Click_Compress_Long_name_file_posix_mode'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsTarPosixTest()
            }
          })

        Text($r('app.string.Click_Compress_Long_name_file_gnu_mode'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsTarGnuTest()
            }
          })
      }
      if (this.isPosixDeCompressTarShow){
        Text($r('app.string.Click_UnCompress_Long_name_file_posix_mode'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsPosixUnTarTest()
            }
          })
      }

      if (this.isGnuDeCompressTarShow){
        Text($r('app.string.Click_UnCompress_Long_name_file_gnu_mode'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsGnuUnTarTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");

      fs.mkdirSync(data + '/' + this.newFolder)
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  generateTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      const writer = fs.openSync(data + '/' + this.newFolder + '/test1.xml', fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
      fs.writeSync(writer.fd, "<?xml version = '1.0'?>\n"
      + "<!DOCTYPE connections>\n"
      + "<connections>\n"
      + "</connections>");
      fs.closeSync(writer);
      AlertDialog.show({ title: $r('app.string.operation_successful'),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newFolder + '/test1.xml',
        confirm: { value: 'OK', action: () => {
          this.isCompressTarFileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  generateLongFileNameTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      let filename = this.getResourceString($r('app.string.longFileName'));
      const writer = fs.openSync(data + '/' + this.newFolder + '/'+filename, fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
      let fileContent = this.getResourceString($r('app.string.longFileNameContent'));
      fs.writeSync(writer.fd, fileContent);
      fs.closeSync(writer);
      AlertDialog.show({ title: $r('app.string.operation_successful'),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newFolder + '/' + filename,
        confirm: { value: 'OK', action: () => {
          this.isLongFileNameCompressTarShow = true
        } }
      });
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  jsTarTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      this.testArArchiveCreation(data as string,undefined,'test1.xml','simple.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/',
        confirm: { value: 'OK', action: () => {
          this.isDeCompressTarShow = true
        } }
      })
    } catch (error) {
    }
  }

  jsTarPosixTest(): void {
    try {
      let data = GlobalContext.getContext().getObject('FilesDir');
      let filename = this.getResourceString($r('app.string.longFileName'));
      this.testArArchiveCreation(data as string, TarArchiveOutputStream.LONGFILE_POSIX, filename, 'posix.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/',
        confirm: { value: 'OK', action: () => {
          this.isPosixDeCompressTarShow = true
        } }
      })
    } catch (error) {
    }
  }

  jsTarGnuTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      let filename = this.getResourceString($r('app.string.longFileName'));
      this.testArArchiveCreation(data as string, TarArchiveOutputStream.LONGFILE_GNU, filename, 'gnu.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/',
        confirm: { value: 'OK', action: () => {
          this.isGnuDeCompressTarShow = true
        } }
      })
    } catch (error) {
    }
  }

  jsUnTarTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      this.testUnCompressTar(data as string, 'simple.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newFolder,
        confirm: { value: 'OK', action: () => {
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }
  jsPosixUnTarTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      this.testUnCompressTar(data as string, 'posix.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newFolder,
        confirm: { value: 'OK', action: () => {
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }
  jsGnuUnTarTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      this.testUnCompressTar(data as string, 'gnu.tar');
      AlertDialog.show({ title: $r("app.string.compression_successful"),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newFolder,
        confirm: { value: 'OK', action: () => {
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  testArArchiveCreation(data: string, LongFileMode: number = TarArchiveOutputStream.LONGFILE_ERROR, filename: string, outputFilename: string) {
    try {
      let output: File = new File(data, outputFilename);
      let file1: File = new File(data + '/' + this.newFolder, filename);
      let input1: InputStream = new InputStream();
      input1.setFilePath(file1.getPath());
      let out: OutputStream = new OutputStream();
      out.setFilePath(output.getPath());
      let os: TarArchiveOutputStream = new TarArchiveOutputStream(out, 512)
      os.setLongFileMode(LongFileMode)
      let entry: TarArchiveEntry = new TarArchiveEntry();
      entry.tarArchiveEntryPreserveAbsolutePath2(outputFilename.replace('.tar','')+"/"+ filename, false);
      entry.setModTime(Long.fromNumber(0));
      entry.setSize(Long.fromNumber(file1.length()));
      entry.setUserId(0);
      entry.setGroupId(0);
      entry.setUserName("avalon");
      entry.setGroupName("excalibur");
      entry.setMode(0o100000);
      os.putArchiveEntry(entry);
      IOUtils.copyStream(input1, os);
      os.closeArchiveEntry();
      os.close();

    } catch (e) {
      console.error("testArArchiveCreation " + e);
    }
  }

  getResourceString(res:Resource){
    return getContext().resourceManager.getStringSync(res.id)
  }

  testUnCompressTar(data: string, intputFileName: string) {
    let input: File = new File(data, intputFileName);
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais: TarArchiveInputStream = new TarArchiveInputStream(input1, TarConstants.DEFAULT_BLKSIZE,
      TarConstants.DEFAULT_RCDSIZE, '', false);
    let tarArchiveEntry: TarArchiveEntry;
    while ((tarArchiveEntry = tais.getNextTarEntry()) != null) {
      let name: string = tarArchiveEntry.getName();
      let tarFile: File = new File(data + '/' + this.newFolder, name);
      if (name.indexOf('/') != -1) {
        try {
          let splitName: string = name.substring(0, name.lastIndexOf('/'));
          fs.mkdirSync(data + '/' + this.newFolder + '/' + splitName);
        } catch (err) {
        }
      }
      let fos: OutputStream | null = null;
      try {
        fos = new OutputStream();
        fos!.setFilePath(tarFile.getPath())
        let read: number = -1;
        let buffer: Int8Array = new Int8Array(tarArchiveEntry.getRealSize().toNumber());
        while ((read = tais.readBytes(buffer)) != -1) {
          fos!.writeBytesOffset(buffer, 0, read);
        }
      } catch (e) {
        throw (e as Error);
      } finally {
        fos!.close();
      }
    }
  }

  shouldReadGNULongNameEntryWithWrongName(data: string): void {
    let input: File = new File(data, "COMPRESS-324.tar");
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais = new TarArchiveInputStream(input1, TarConstants.DEFAULT_BLKSIZE,
      TarConstants.DEFAULT_RCDSIZE, '', false);
    let tarArchiveEntry: TarArchiveEntry = tais.getNextTarEntry();
    tarArchiveEntry.getName();
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}