/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { snappyCompress, snappyUncompress } from '@ohos/commons-compress';
import fs from '@ohos.file.fs';
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
struct SnappyTest {
  @State newfolder: string = 'newfolder'
  @State newfile: string = 'bla.txt'
  @State newfile1: string = 'bla1.txt'
  @State isCompressSnappyFileShow: boolean = false;
  @State isDeCompressSnappyShow: boolean = false;
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text($r('app.string.Snappy_functions'))
        .fontSize(20)
        .margin({ top: 16 })

      Text($r('app.string.generate_bla_txt'))
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.createFile()
          }
        })

      if (this.isCompressSnappyFileShow) {
        Text($r('app.string.compress_txt_as_sz'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.snappyJsTest(true)
            }
          })
      }


      if (this.isDeCompressSnappyShow) {
        Text($r('app.string.decompress_sz'))
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.snappyJsTest(false)
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    this.createFolder()
  }

  createFolder() {
    try {
      try {
        let data = GlobalContext.getContext().getObject("FilesDir");

        fs.mkdirSync(data + '/' + this.newfolder)
      } catch (err) {
      }
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  getResourceString(res:Resource){
    return getContext().resourceManager.getStringSync(res.id)
  }

  createFile() {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      let writer = fs.openSync(data + '/' + this.newfile, fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
      fs.writeSync(writer.fd, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
      );
      fs.closeSync(writer);
      AlertDialog.show({ title: $r('app.string.operation_successful'),
        message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newfile,
        confirm: { value: 'OK', action: () => {
          this.isCompressSnappyFileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  snappyJsTest(value:boolean) {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      if (value) {
        let path = data + '/' + this.newfile
        console.log('snappyCompress');
        snappyCompress(path, path + '.sz')
          .then(() => {
            AlertDialog.show({ title: $r("app.string.compression_successful"),
              message: this.getResourceString($r('app.string.view_sandbox_path')) + path + '.sz',
              confirm: { value: 'OK', action: () => {
                this.isDeCompressSnappyShow = true
              } }
            })
          });
      } else {
        snappyUncompress(data + '/' + this.newfile + '.sz', data + '/' + this.newfile1)
          .then(() => {
            AlertDialog.show({ title: $r("app.string.decompression_successful"),
              message: this.getResourceString($r('app.string.view_sandbox_path')) + data + '/' + this.newfile1,
              confirm: { value: 'OK', action: () => {
              } }
            })
          });
      }
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}
