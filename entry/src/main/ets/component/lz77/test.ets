/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {
  LZ77Compressor,
  Block,
  LiteralBlock,
  Long,
  InputStream,
  AbstractLZ77CompressorInputStream,
  ByteArrayInputStream,
  ByteUtils,
  Arrays,
  System,
  subParameters
} from '@ohos/commons-compress';

@Entry
@Component
struct Lz77Test {
  TAG: string = 'lz77Test----'
  scroller: Scroller = new Scroller()
  @State isCompressLz77FileShow: boolean = false;
  @State isDeCompressLz77Show: boolean = false;
  @State lz77Result: string = "lz77Test";
  preTimestamp: number = 0;

  build() {
    Stack({ alignContent: Alignment.TopStart }) {
      Scroll(this.scroller) {
        Column() {
          Text($r('app.string.lz77_function'))
            .fontSize(20)
            .margin({ top: 16 })

          Text($r('app.string.test_parameters'))
            .fontSize(16)
            .margin({ top: 32 })
            .padding(8)
            .border({ width: 2, color: '#535353', radius: 6 })
            .onClick((event) => {
              if (!this.isFastClick()) {
                this.testParameters()
              }
            })

          if (this.isCompressLz77FileShow) {
            Text($r('app.string.test_abstract'))
              .fontSize(16)
              .margin({ top: 32 })
              .padding(8)
              .border({ width: 2, color: '#535353', radius: 6 })
              .onClick((event) => {
                if (!this.isFastClick()) {
                  let timer1 = System.currentTimeMillis()
                    .toString()
                  console.info(this.TAG + timer1)

                  this.testAbstractLZ77CompressorInputStream()

                  let timer2: string = System.currentTimeMillis()
                    .toString()
                  console.info(this.TAG + timer2)
                }
              })
          }

          if (this.isDeCompressLz77Show) {
            Text($r('app.string.test_LZ77Compressor'))
              .fontSize(16)
              .margin({ top: 32 })
              .padding(8)
              .border({ width: 2, color: '#535353', radius: 6 })
              .onClick((event) => {
                if (!this.isFastClick()) {
                  this.testLZ77Compressor()
                }
              })
          }

          Text($r('app.string.test_result') + this.lz77Result)
            .fontSize(16)
            .margin({ top: 32 })
            .padding(8)
        }
        .width('100%')
      }
      .scrollable(ScrollDirection.Vertical)
      .scrollBar(BarState.On)
      .scrollBarColor(Color.Gray)
      .scrollBarWidth(30)
      .onScrollEdge((side: Edge) => {
        console.info('To the edge')
      })
    }
    .width('100%')
    .height('100%')
    .backgroundColor(0xDCDCDC)

  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }

  newParameters(windowSize: number, minBackReferenceLength: number, maxBackReferenceLength: number,
                maxOffset: number, maxLiteralLength: number): subParameters {
    return subParameters.builder(windowSize)
      .withMinBackReferenceLength(minBackReferenceLength)
      .withMaxBackReferenceLength(maxBackReferenceLength)
      .withMaxOffset(maxOffset)
      .withMaxLiteralLength(maxLiteralLength)
      .build();
  }

  newParameters1(windowSize: number): subParameters {
    return subParameters.builder(windowSize).build();
  }

  testParameters() {
    let p: subParameters = this.newParameters1(128);
    let p1: subParameters = this.newParameters(128, 2, 3, 4, 5);
    if (p.getWindowSize() == 128 || p.getMaxOffset() == 127 || p1.getMaxBackReferenceLength() == 3) {
      AlertDialog.show({ title: $r('app.string.successful'),
        message: '',
        confirm: { value: 'OK', action: () => {
          this.isCompressLz77FileShow = true
        } }
      })
      this.lz77Result = JSON.stringify(p)
    }
  }

  testAbstractLZ77CompressorInputStream() {
    let data: Int8Array = new Int8Array([1, 2, 3, 4]);
    let s: TestStream = new TestStream(new ByteArrayInputStream(ByteUtils.EMPTY_BYTE_ARRAY, 0, 0))
    s.prefill(data);
    s.startBackReference(2, Long.fromNumber(4));
    let r: Int8Array = new Int8Array(4);
    if (s.readBytes(r) == 4) {
      AlertDialog.show({ title: $r('app.string.successful'),
        message: '',
        confirm: { value: 'OK', action: () => {
          this.isDeCompressLz77Show = true
        } }
      })
      this.lz77Result = JSON.stringify(s)
    }
  }

  compress(params: subParameters, chunks: Int8Array): Array<Block> {
    let blocks: Array<Block> = new Array<Block>();
    let c: LZ77Compressor = new LZ77Compressor(params, (block) => {
      if (block instanceof LiteralBlock) {
        let b: LiteralBlock = block as LiteralBlock;
        let len: number = b.getLength();
        block = new LiteralBlock(
          Arrays.copyOfRangeInt(new Int32Array(b.getData()), b.getOffset(), b.getOffset() + len),
          0, len);
      }
      blocks.push(block);
    });

    c.compressByte(chunks);
    c.finish();
    return blocks;
  }

  testLZ77Compressor() {
    let a: string = "Blah blah blah blah blah!"
    let c:number[] = []
    for (let i = 0; i < a.length; i++) {
      let b = a.charCodeAt(i)
      c.push(b as never)
    }
    let BLA: Int8Array = new Int8Array(c);
    let blocks = this.compress(this.newParameters1(128), BLA)
    if (blocks.length == 4) {
      this.lz77Result = JSON.stringify(blocks)
      AlertDialog.show({ title: $r('app.string.successful'),
        message: '',
        confirm: { value: 'OK', action: () => {

        } }
      })
    }
  }
}

class TestStream extends AbstractLZ77CompressorInputStream {
  private literal: boolean = false;

  constructor(input: InputStream) {
    super(input, 1024);
  }

  public readBytesOffset(b: Int8Array, off: number, len: number): number {
    if (this.literal) {
      return this.readLiteral(b, off, len);
    }
    return this.readBackReference(b, off, len);
  }

  literal1(len: number): void {
    this.startLiteral(Long.fromNumber(len));
    this.literal = true;
  }
}