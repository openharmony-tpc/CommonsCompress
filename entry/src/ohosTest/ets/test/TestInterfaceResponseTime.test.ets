/**
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { afterAll, afterEach, beforeAll, beforeEach, describe, expect, it, TestType } from '@ohos/hypium'
import { GlobalContext } from './archivers/ar/GlobalContext';
import { gzipFile, unGzipFile } from '@ohos/commons-compress';
import { lz4Compressed, lz4Decompressed, System } from '@ohos/commons-compress';
import { snappyCompress, snappyUncompress } from '@ohos/commons-compress';
import { DeflateFile, InflateFile } from '@ohos/commons-compress';

export default function telephonyPerfJsunit(){
  describe("telephonyPerfJsunit", ()=>{
    const BASE_COUNT=2000
    const HTTP_COUNT=2
    const BASELINE_HASSIMECASR=500
    const BASELINE_CREATEHTTP=500
    const BASELINE_REQUEST=2500
    const BASELINE_DESTROY=30

    //SVGCircle
    it("gzipFile", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_gzipFile startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data = GlobalContext.getContext().getObject("FilesDir");
        console.info('directory obtained. Data:' + data);
        gzipFile(data + '/hello.txt', data + '/test.txt.gz')
          .then((isSuccess) => {
            if (isSuccess) {
              // AlertDialog.show({ title: '压缩成功',
              //   message: '请查看沙箱路径 ' + data + '/test.txt.gz',
              //   confirm: { value: 'OK', action: () => {
              //     this.isDeCompressGZipShow = true
              //   } }
              // })
            }
          });
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_gzipFile endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_gzipFile averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    //SVGDeclares
    it("unGzipFile", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_unGzipFile startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data = GlobalContext.getContext().getObject("FilesDir");
        unGzipFile(data + '/test.txt.gz', data + '/test.txt')
          .then((isSuccess) => {
            if (isSuccess) {
              AlertDialog.show({ title: '解缩成功',
                message: '请查看沙箱路径 ' + data + '/test.txt',
                confirm: { value: 'OK', action: () => {
                } }
              })
            }
          });
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_unGzipFile endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_unGzipFile averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    //SVGCircle
    it("lz4Compressed", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Compressed startTime:"+startTime);
      let data = GlobalContext.getContext().getObject("FilesDir");

      for (let index = 0; index < BASE_COUNT; index++) {
        lz4Compressed(data + '/test.txt', data + '/test.txt.lz4')

      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Compressed endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Compressed averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    it("lz4Decompressed", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Decompressed startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath:string = data + "/new"
        let src001:string = srcPath + "/hello.txt";
        let deflateDest001:string = data + "/newFolder.txt.lz4";

        let isSuccess = await lz4Compressed(src001, deflateDest001)
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Decompressed endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_lz4Decompressed averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    it("snappyCompress", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyCompress startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath:string = data + "/new"
        let src001:string = srcPath + "/hello.txt";
        let snappyDest001:string = data + "/newFolder.txt.sz";

        let isSuccess = await snappyCompress(src001, snappyDest001)
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyCompress endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyCompress averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    it("snappyUncompress", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyUncompress startTime:"+startTime);
      let data =GlobalContext.getContext().getObject("FilesDir");
      let snappyDir001 = data + "/newFolder.txt.sz";
      let snappyNew001 = data + "/newTarget";
      let snappyDest001 = snappyNew001 + "/newFolder1.txt";

      for (let index = 0; index < BASE_COUNT; index++) {
        let undeflateIsSuccess = await snappyUncompress(snappyDir001, snappyDest001)
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyUncompress endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_snappyUncompress averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    it("DeflateFile", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_DeflateFile startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath:string = data + "/new"
        let src001:string = srcPath + "/hello.txt";
        let deflateDest001:string = data + "/newFolder.txt.deflate";
        let isSuccess = await DeflateFile(src001, deflateDest001)
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_DeflateFile endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_DeflateFile averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

    it("InflateFile", TestType.PERFORMANCE, async (done:Function)=> {
      let startTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_InflateFile startTime:"+startTime);

      for (let index = 0; index < BASE_COUNT; index++) {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let undeflateDir001:string = data + "/newFolder.txt.deflate";
        let undeflateNew001:string = data + "/newTarget";
        let undeflateDest001:string = undeflateNew001 + "/newFolder.txt";

        let undeflateIsSuccess = await InflateFile(undeflateDir001, undeflateDest001)
      }

      let endTime=new Date().getTime()
      console.log("Telephony_Http_CreateHttp_Perf_0100_InflateFile endTime:"+endTime);

      let averageTime=((endTime-startTime)*1000)/BASE_COUNT
      console.log("Telephony_Http_CreateHttp_Perf_0100_InflateFile averageTime:"+averageTime+"μs");
      expect(averageTime<BASELINE_CREATEHTTP).assertTrue();
      done()
    });

  })
}