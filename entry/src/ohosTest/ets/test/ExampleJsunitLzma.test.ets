/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fs from '@ohos.file.fs';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import { InputStream, OutputStream, Exception, Decoder, Encoder, Long } from '@ohos/commons-compress';
import { GlobalContext } from '../testability/gloalThis';

export default function lzmaTest() {
  describe('lzmaCompressTest', ()=> {

    it('lzma_compress_test', 0, (done:Function)=> {
      try {
        let data =GlobalContext.getContext().getObject("FilesDir");
        let srcPath:string = data + "/new";
        let src001:string = srcPath + "/hello.txt";
        let lzmaDest001:string = data + "/newFolder.lzma";
        let unlzmaDir001:string = data + "/newFolder.lzma";
        let unlzmaDest001:string = srcPath + "/hello1.txt";
        try {
          fs.mkdirSync(srcPath);
        } catch (err) {
        }
        const writer = fs.openSync(src001, fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
        fs.writeSync(writer.fd, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
        + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
        );
        fs.closeSync(writer)
        lzmaCompress(src001, lzmaDest001)
        let lzmaStat = fs.statSync(lzmaDest001);
        let isFile = lzmaStat.isFile()
        expect(isFile).assertTrue()
        let srcSize = fs.statSync(src001).size;
        let lzmaDestSize = lzmaStat.size;
        expect(srcSize >= lzmaDestSize).assertTrue();
        lzmaUncompress(unlzmaDir001, unlzmaDest001)
        let unlzmaStat = fs.statSync(unlzmaDest001);
        let unlzmaDestSize = unlzmaStat.size;
        let originSize = fs.statSync(src001).size;
        let unlzmaResult = (originSize == unlzmaDestSize);
        expect(unlzmaResult).assertTrue();
        done()
      } catch (err) {
        console.error('lzma_compress_test err:' + err);
        done();
      }
    })
  })
}

function lzmaCompress(inputPath:string, outputPath:string) {
  let inputStream1: InputStream = new InputStream();
  let outputStream1: OutputStream = new OutputStream();
  outputStream1.setFilePath(outputPath)
  inputStream1.setFilePath(inputPath);
  let stat = fs.statSync(inputPath);
  let encoder: Encoder = new Encoder();
  if (!encoder.SetAlgorithm(2))
    return
    if(!encoder.SetDictionarySize(1 << 23))
  return
  if(!encoder.SetNumFastBytes(128))
  return
  if(!encoder.SetMatchFinder(1))
  return
  if(!encoder.SetLcLpPb(3, 0, 2))
  return
  encoder.SetEndMarkerMode(false);
  encoder.WriteCoderProperties(outputStream1);
  let fileSize: Long = Long.fromNumber(stat.size);
  for (let i = 0; i < 8; i++) {
    outputStream1.write(fileSize.shiftRightUnsigned(8 * i).toInt() & 0xFF);
  }
  encoder.Code(inputStream1, outputStream1, Long.fromNumber(-1), Long.fromNumber(-1), null);
  outputStream1.flush();
  outputStream1.close();
  inputStream1.close();
}

function lzmaUncompress(inputPath:string, outputPath:string) {
  let decoder: Decoder = new Decoder();
  let inputStream2: InputStream = new InputStream();
  inputStream2.setFilePath(inputPath)
  let outputStream2: OutputStream = new OutputStream();
  outputStream2.setFilePath(outputPath)
  let propertiesSize = 5;
  let properties: Int8Array = new Int8Array(propertiesSize);
  if (inputStream2.readBytesOffset(properties, 0, propertiesSize) != propertiesSize)
    throw new Exception("input .lzma file is too short");
  if (!decoder.SetDecoderProperties(properties))
    throw new Exception("Incorrect stream properties");
  let outSize: Long = Long.fromNumber(0);
  for (let i = 0; i < 8; i++) {
    let v: number = inputStream2.read();
    if (v < 0)
      throw new Exception("Can't read stream size");
    outSize = outSize.or(Long.fromNumber(v).shiftLeft(8 * i));
  }
  if (!decoder.Code(inputStream2, outputStream2, outSize))
    throw new Exception("Error in data stream");
  outputStream2.flush();
  outputStream2.close();
  inputStream2.close();
}