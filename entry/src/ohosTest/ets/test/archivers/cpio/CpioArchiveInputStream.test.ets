import fs from '@ohos.file.fs';
import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium'
import {
  CpioArchiveInputStream,
  InputStream,
  OutputStream,
  ArchiveOutputStream,
  ArchiveStreamFactory,
  CpioArchiveEntry,
  StringBuilder,
  IOUtils,
  File,
  ArchiveEntry,
  Long
} from '@ohos/commons-compress';
import { GlobalContext } from '../../../testability/gloalThis';

/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export default function CpioArchiveInputStreamTest() {
  describe('cpioArchiveInputStreamTest', ()=> {

    it('cpioArchiveInputStreamTest1', 0, (done:Function)=> {
      let data =GlobalContext.getContext().getObject("FilesDir");
      testCpioUnarchive(data as string)
      done()
    })

    it('cpioArchiveInputStreamTest2', 0, (done:Function)=> {
      let data =GlobalContext.getContext().getObject("FilesDir");
      testCpioUnarchiveCreatedByRedlineRpm(data as string)
      done()
    })


    it('cpioArchiveInputStreamTest3', 0, (done:Function)=> {
      let data =GlobalContext.getContext().getObject("FilesDir");
      singleByteReadConsistentlyReturnsMinusOneAtEof(data as string)
      done()
    })

    it('cpioArchiveInputStreamTest4', 0, (done:Function)=> {
      let data =GlobalContext.getContext().getObject("FilesDir");
      multiByteReadConsistentlyReturnsMinusOneAtEof(data as string)
      done()
    })
  })
}


function testCpioUnarchive(data:string): void {

  const writer = fs.openSync(data + '/test1.xml', fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
  fs.writeSync(writer.fd, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fs.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copyStream(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let expected: StringBuilder = new StringBuilder();
  expected.append("./test1.xml<?xml version=\"1.0\"?>\n");
  expected.append("<empty/>./test2.xml<?xml version=\"1.0\"?>\n");
  expected.append("<empty/>\n");
  let path = "/data/data/com.mykey.myapp/cache/test.xml";


  let result: StringBuilder = new StringBuilder();
  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");

  let entry: CpioArchiveEntry;

  while ((entry = s.getNextEntry()) != null) {
    result.append(entry.getName());
    let tmp: number;
    while ((tmp = s.read()) != -1) {
      result.append(tmp);
    }
  }
  expect(expected.toString() == result.toString())

}


function testCpioUnarchiveCreatedByRedlineRpm(data:string) {

  const writer = fs.openSync(data + '/test1.txt', fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
  fs.writeSync(writer.fd, "Hello World!");
  fs.closeSync(writer);

  let output: File = new File(data, "redline.cpio");
  let file1: File = new File(data, "test1.txt");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.txt", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copyStream(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let count: number = 0;
  let inFile: File = new File(data, "redline.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");

  let entry: CpioArchiveEntry | null = null;

  while ((entry = s.getNextEntry()) != null) {
    count++;

  }

  expect(count).assertEqual(1)

}


function singleByteReadConsistentlyReturnsMinusOneAtEof(data:string) {
  const writer = fs.openSync(data + '/test1.xml', fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
  fs.writeSync(writer.fd, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fs.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copyStream(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();

  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");
  let e: ArchiveEntry = s.getNextEntry();

  IOUtils.toByteArray(s);

  expect(s.read()).assertEqual(-1)

  expect(s.read()).assertEqual(-1)
}


function multiByteReadConsistentlyReturnsMinusOneAtEof(data:string) {
  const writer = fs.openSync(data + '/test1.xml', fs.OpenMode.WRITE_ONLY | fs.OpenMode.CREATE);
  fs.writeSync(writer.fd, "<?xml version = '1.0'?>\n"
  + "<!DOCTYPE connections>\n"
  + "<connections>\n"
  + "</connections>");
  fs.closeSync(writer);


  let output: File = new File(data, "bla.cpio");
  let file1: File = new File(data, "test1.xml");
  let inputStream1 = new InputStream();
  inputStream1.setFilePath(file1.getPath());
  let out: OutputStream = new OutputStream();
  out.setFilePath(output.getPath());
  let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("cpio", out);
  let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
  archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
  os.putArchiveEntry(archiveEntry1);
  IOUtils.copyStream(inputStream1, os);
  os.closeArchiveEntry();

  os.close();
  out.close();


  let buf: Int8Array = new Int8Array(2);
  let inFile: File = new File(data, "bla.cpio");
  let input: InputStream = new InputStream();
  input.setFilePath(inFile.getPath());
  let s: CpioArchiveInputStream = new CpioArchiveInputStream(input, 512, "US-ASCII");
  let e: ArchiveEntry = s.getNextEntry();
  IOUtils.toByteArray(s);

  expect(s.readBytes(buf)).assertEqual(-1)
  expect(s.readBytes(buf)).assertEqual(-1)
}



     



